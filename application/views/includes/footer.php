<?php

if(isset($_POST['submit']))
{
   $FirstName  = $_POST['first_name'];
   $LastName = $_POST['last_name'];
   $Email = $_POST['email'];
   $ContactNumber = $_POST['phone_number'];
   $message = $_POST['message'];
   if($FirstName == '' || $LastName == '' || $Email == '' || $message == '')
   {
       echo "All fields sare required";
   }
   else{
       $from = "DBS Trading";
       $fromEmail = "info@dbstrading.co.uk";
       $subject = "Contact Us Form from DBS Trading";
       $to = $Email;

       $headers = "From: ". $from."<".$fromEmail.">\r\n";
       $headers .= "X-Mailer: PHP/". phpversion(). "\r\n";
       $headers .= "MIME-Version: 1.0". "\r\n";
       $headers .= "Content-Type: text/html; charset-ISO-8859-l\r\n";

       $message = "<html><body>";
       $message .= "<p>Name: ". $FirstName.$LastName."</p>";
       $message .= "<p>Email: ". $Email."</p>";
       $message .= "<p>Phone Number: ". $ContactNumber."</p>";
       $message .= "<p>Message: ". $message."</p>";

       $sendmail = mail($to,$subject,$subject,$headers);

       echo "You have successfuly sent you email";
   }
}

?>

<section class="contact-us py-3">
  <div class="container">
    <div class="row">
      <div class="col-8 p-5 form-deign">
        <form action="footer.php" method="post">
          <div class="row">
            <div class="col-12">
              <h4>Send Us A Message</h4>
            </div>
            <div class="col-6 pb-2">
              <input name="first_name" type="text" class="btn-block" placeholder="First Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="last_name" type="text" class="btn-block" placeholder="Last Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="email" type="text" class="btn-block" placeholder="Email *">
            </div>
            <div class="col-6 pb-2">
              <input name="phone_number" type="text" class="btn-block" placeholder="Your Phone (optional)">
            </div>
            <div class="col-12 pb-2">
              <textarea name="message" id="" cols="10" rows="5" class="btn-block" placeholder="Your Message *"></textarea>
            </div>
            <div class="col-3 pb-2">
              <button class="btn btn-dark">SEND MESSAGE</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-4 contact-info">
        <div class="row">
          <div class="col-2 pt-5">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-headphones fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-5">
              <p>CUSTOMMER SUPPORT</p>
              <p>+44 113 344 0171</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-envelope-o fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>EMAIL ADDRESS</p>
              <p>contact@dbstrading.co.uk</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-map-marker  fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>OFFICE ADDRESS</p>
              <p>43 Church Lane,Pudsey,
                Leeds,West Yorkshire, 
                UK LS28 7RR</p>
          </div>

        </div>
      </div>
    </div>

  </div>
</section>

<footer class="footer-section py-5">
    
  <div class="container">
  <div class="row">
    <div class="col-3">
     <a href=""> <img src="assets/img/logo.png" alt="" style="width: 80%;"></a>
    </div>
    <div class="col-3 text-white">
      <h5>SUPPORT</h5>
      <ul class="footer-links">
        <li><a href="">FAQs</a></li>
        <li><a href="">Getting Started</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
    <h5>INFORMATION</h5>
    <ul class="footer-links">
        <li><a href="">Data Protection Policy</a></li>
        <li><a href="">Privacy Policy</a></li>
        <li><a href="terms-condition.php">Terms and Conditions</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
      <img src="assets/img/Cyber-Essentials-Badge-Large-72dpi.png" alt="" style="width: 50%;">
      <img src="assets/img/CPD-Member-White.png" alt="" style="width: 50%;">
    </div>
  </div>

  <div class="row">
    <div class="col-12 text-center text-white">
      <p>© Copyright. All rights reserved. DBS Trading</p>
    </div>
  </div>





  </div>


</footer>