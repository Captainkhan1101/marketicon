
<footer class="footer-section py-5">
  <div class="container">
  <div class="row">
    <div class="col-3">
     <a href="index.php"> <img src="assets/img/marketicon.png" alt="" style="width: 80%;"></a>
    </div>
    <div class="col-3 text-white">
      <h5>SUPPORT</h5>
      <ul class="footer-links">
        <li><a href="<?php echo base_url();?>faq">FAQs</a></li>
        <li><a href="<?php echo base_url();?>apply_now">Apply Now</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
    <h5>INFORMATION</h5>
    <ul class="footer-links">
        <li><a href="<?php echo base_url();?>dataProtection">Data Protection Policy</a></li>
        <li><a  href="<?php echo base_url();?>privacy-policy">Privacy Policy</a></li>
        <li><a href="<?php echo base_url();?>terms-and-condition">Terms and Conditions</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
      <img src="assets/img/Cyber-Essentials-Badge-Large-72dpi.png" alt="" style="width: 50%;">
    </div>
  </div>

  <div class="row">
    <div class="col-6 text-white">
      <p>© Copyright. All rights reserved. Marketicon.</p> <span></span>
    </div>
    <div class="col-6 text-center text-white">
      <p>Company number: 12691478</p> <span></span>
    </div>
  </div>
  </div>
</footer>


    <!-- JS here -->
    <!-- All JS Custom Plugins Link Here here -->
    <script src="<?php echo base_url();?>assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="<?php echo base_url();?>assets/js/jquery.slicknav.min.js"></script>
    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/slick.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/price_rangs.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/animated.headline.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>
    <!-- Scrollup, nice-select, sticky -->
    <script src="<?php echo base_url();?>assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.nice-select.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>
    <!-- contact js -->
    <script src="<?php echo base_url();?>assets/js/contact.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.form.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/mail-script.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.ajaxchimp.min.js"></script>
    <!-- Jquery Plugins, main Jquery -->
    <script src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // show the alert
            setTimeout(function() {
                $(".alert").alert('close');
            }, 12000);
        });
    </script>
<script type="text/javascript">
        if(!(document.cookie)){
            $(document).ready(function () {
                $('#cookieModal').modal('show');
            });
        }
        function accept_cookie()
        {
            $.ajax({
                url: '<?php echo base_url();?>HomeController/set_user_cookie',
                cache: false,
                success: function(html){
                }
            });
        }
    </script>
</body>
</html>