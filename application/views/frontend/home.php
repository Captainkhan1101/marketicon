<!DOCTYPE html>
<html lang="en">
<body>

<?php include_once('includes/header.php') ?>

<div class="banner">
  <div class="col-12 text-center main-heading px-5">
    <h1 style="color: white;"><span style="color: #00bb7e;">Free DBS Check:</span> Disclosure and Barring Service</h1>
  </div>

  <div class="container">
    <div class="row py-5">

      <div class="col-12 fg-heading text-center">
        <p>We provide Disclosure and Barring Service (DBS) checks, to help organisations undertake security checks of those working for them, ensuring that their customers are protected from harm.</p>

        <div class="row py-5">
          <div class="col-6 offset-md-3">
            <a href="<?php echo base_url();?>apply_now"><button class="btn btn-danger btn-block p-3">Apply Now</button></a>
          </div>
        </div>
      </div>
    </div>

  </div>


</div>

<section class="about-section py-5">
  <div class="container text-center">
    <p>We provide Disclosure and Barring Service (DBS) checks, to help organisations undertake security checks of those working for them, ensuring that their customers are protected from harm.
    We can also provide individual support on a face to face basis, and develop specific solutions to individual challenges faced by you (including training courses).
    </p>
    <p>There are three types of DBS checks, which are only available for applicants who are 16 or over:</p>
    <h2>A DBS check is a legal requirement</h2>
    <ul style="list-style-type: circle; text-align: left; padding-right: 30px;">
      <li>for certain jobs or voluntary work, for example working with children or in healthcare</li>
      <li>to foster or adopt a child</li>
    </ul>


    <h2>There are three types of DBS checks, which are only available for applicants who are 16 or over:</h2>
    <ul style="list-style-type: circle; text-align: left; padding-right: 30px;">
      <li>Standard - This will check for spent and unspent convictions, cautions, reprimands and final warnings.</li>
      <li>Enhanced - This includes the same as the standard check plus any additional information held by local police that is reasonably considered relevant to the <a href="https://www.gov.uk/government/publications/dbs-workforce-guidance">workforce</a> being applied for (adult, child or ‘other’ workforce). ‘Other’ workforce means those who don’t work with children or adults specifically, but potentially both, eg taxi drivers. In this case, the police will only release information that is relevant to the post being applied for. </li>
      <li>Enhanced with list checks - This is like the enhanced check, but includes a check of the DBS <a href="https://www.gov.uk/disclosure-barring-service-check/dbs-barred-lists">barred lists</a>. An employer can only ask for a barred list check for <a href="https://www.gov.uk/government/publications/dbs-check-eligible-positions-guidance">specific roles</a>. It’s a criminal offence to ask for a check for any other roles</li>
    </ul>

    <!-- <div class="row">
      <div class="col-6 text-center">
        <button class="btn btn-success">Online Trainiing Courses</button>
      </div>
      <div class="col-6 text-center">
        <button class="btn btn-success ">Our CX Products</button>
      </div>
    </div> -->
  </div>
</section>

<!-- <section class="DBS-check text-center">
  <div class="container">
    <h2>What are DBS Checks?</h2>
    <p>DBS (Disclosure and Barring Service) checks are requests to the UK police service to disclose any information they hold on an applicant. This allows you to know an individual's criminal record if they have one. <br>
      They are often carried out by people who are employing staff, and if this is the case for you, more information can be viewed on our <span style="color: #00bb7e;">DBS Checks for Employers</span> page. <br>
      There are three levels of disclosure: <span style="color: #00bb7e;">Basic DBS check</span>,
      <span style="color: #00bb7e;">Standard DBS check</span>, and <span style="color: #00bb7e;">Enhanced DBS check</span>.
    </p>
    <div class="col-12 mb-3">
      <img src="assets/img/asset1.jpg" alt="" style="width: 100%;">
    </div>

    <button class="btn btn-danger">Regiser for free today!</button>

  </div>
</section> -->
  <!-- footer section  -->
 
  <section class="contact-us py-3">
<?php if($this->session->flashdata('flash_msg_yes')){ ?>

<div class="alert alert-success alert-dismissible " role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
</button>
<strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
</div>

<?php }?>

<?php if($this->session->flashdata('flash_msg_no')){ ?>

<div class="alert alert-danger alert-dismissible " role="alert">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
</button>
<i class="fa fa-exclamation-triangle"></i> <?php echo $this->session->flashdata('flash_msg_no'); ?>
</div>

<?php }?>
  <div class="container">
    <div class="row">
      <div class="col-8 p-5 form-deign col-sm-7">
        <form action="<?php echo base_url(); ?>FrontendController/contactmailHome" method="post">
          <div class="row">
            <div class="col-12">
              <h4>Send Us A Message</h4>
            </div>
            <div class="col-6 pb-2">
              <input name="first_name" type="text" class="btn-block" placeholder="First Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="last_name" type="text" class="btn-block" placeholder="Last Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="email" type="text" class="btn-block" placeholder="Email *">
            </div>
            <div class="col-6 pb-2">
              <input name="phone_number" type="text" class="btn-block" placeholder="Your Phone (optional)">
            </div>
            <div class="col-12 pb-2">
              <textarea name="message" id="" cols="10" rows="5" class="btn-block" placeholder="Your Message *"></textarea>
            </div>
            <div class="col-3 pb-2 col-sm-6">
              <button class="btn btn-dark" type="submit" name="submit">SEND MESSAGE</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-4 contact-info col-sm-5">
        <div class="row">
          <div class="col-2 pt-5">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-headphones fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-5">
              <p>CUSTOMMER SUPPORT</p>
              <p>02080883560</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-envelope-o fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>EMAIL ADDRESS</p>
              <p>contact@marketicon.co.uk</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-map-marker  fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>OFFICE ADDRESS</p>
              <p>Spaces Heathrow Airport
                4 Roundwood Avenue
                Stockley Park
                UXBRIDGE
                UB11 1AF
                </p>
          </div>

        </div>
      </div>
    </div>

  </div>
</section>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>