<?php  include_once('includes/header.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-12 heading-text text-center py-5">
      <h3>Who We Are and Our Online DBS Checks</h3>
    </div>
  </div>

  <div class="row">
    <div class="col-8 text-center">
      <h2>Get Your Staff Into Work Faster With Our Online DBS Checks</h2>
      <p class="para">Aaron's Department was set up to provide in house IT, administration and software solutions to businesses operated by Personnel Limited (Registered limited company in England, 29th December 2003 No 5004030) and Personnel Recruitment Services Limited (Registered limited company in England 06th August 1998 No 3611213). <br> <br>
      In 2002, we set up Aaron's Department DBS, a registered Umbrella Body, authorised by the UK Government to act as an intermediary between the Disclosure and Barring Service and an employer or organisation. Over the years we have grown from strength to strength and process thousands of applications every year for our clients. <br> <br>
      We launched our Online DBS system in 2015, making it even easier for people and companies to apply for DBS Checks. The online service is superior to the traditional method of paper applications, and our fastest online application from start to a returned certificate in 2021 was 1 Day, 0 Hours, 30 Minutes, 45 Seconds.
      </p>
    </div>
    <div class="col-4 contact-info">
        <div class="row">
          <div class="col-2 pt-5">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-headphones fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-5">
              <p>CUSTOMMER SUPPORT</p>
              <p>+44 113 344 0171</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-envelope-o fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>EMAIL ADDRESS</p>
              <p>contact@aaronsdepartment.com</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-map-marker  fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>OFFICE ADDRESS</p>
              <p>43 Church Lane,Pudsey,
                Leeds,West Yorkshire, 
                UK LS28 7RR</p>
          </div>

        </div>
      </div>
  </div>

  <div class="row">
    <div class="col-12">
      <button class="btn btn-danger">REGISTER NOW</button>
    </div>
  </div>

  <div class="row">
    <div class="col-8 py-5">
      <h3 class="text-center">CPD Accredited Online Training Courses</h3>
      <p class="para">After the Coronavirus pandemic hit, and social distancing measure came into place, we decided to create a range of online training courses for businesses and end-users to complete.</p>
      <p class="para">We have a comprehensive list of CPD Accredited Online Training Courses for healthcare professionals and businesses to learn afresh, or to top up their knowledge.</p>
      <p class="para"> These can be viewed on the Online Training Courses Page:</p>

      <div class="col-12 text-center">
        <button class="btn btn-success p-3">Online Training Courses</button>
      </div>
    </div>
   
  </div>

</div>


<?php include_once('includes/footer.php') ?>
</body>
</html>