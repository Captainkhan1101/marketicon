

<div class="container">
  <div class="row">
    <div class="col-12 heading-text py-5">
      <h1 class="text-center">Terms and Condition</h1>
      <p><strong>Definitions:</strong> This Service and website is operated by Personnel Limited Company Number 5004030. Herein after called "Personnel", "we", "us" or "our". "You", "Your" and "User" means the legal entity subject to these terms and conditions, whether an individual, a company or any other legal entity, and includes anyone reasonably appearing to us to be acting with your authority or permission. "Marketicon" or "Service", means the Marketicon website and the Marketicon Software System collectively.</p>
      <h5>The following terms and conditions govern your use of Marketicon:</h5>
      <p><strong>1. Provisions:</strong>
        Each of the provisions contained in the Terms and Conditions shall be construed as being independent of every other, so that if any provision is held to be invalid or unenforceable by a court of competent authority, such determination shall not affect the validity of any other terms or conditions or stipulation detailed herein, which shall remain in full force and effect.</p>
        <p><strong>2. Modifications of Terms:</strong>
          Personnel reserves the right to vary these terms and conditions from time to time. Any modifications of these conditions may be superseded in writing only by Personnel and may be made without notifying or consulting you and they shall have precedence over conditions coming from you. You are advised to check the Marketicon website Monthly to ascertain the prevailing terms and conditions. Use of Marketicon for a period exceeding two weeks after changes to the Terms and Conditions have been published will constitute acceptance.</p>
        <p><strong>3. Copyrights and Intellectual Property Rights:</strong>
          The ownership of all intellectual property rights including, for the avoidance of doubt, patents, copyright, trade marks and trade names in Marketicon belong or are licensed to Personnel and any part of Marketicon may not be transferred, copied, decompiled or reproduced in whole or in part in without the express permission in writing of Personnel except as is expressly provided in Section 50B of the Copyright Design and Patent Act. 1988.</p>
        <p><strong>4 Disclaimer of Warranties:</strong>
          Personnel take all reasonable care to ensure that Marketicon is kept up to date, we make no warranty or representation that any part is Personnel or reliable, or that it will always be available, or that any problems will be corrected, or that it is free of viruses, errors and defects. If you download software or other material from Marketicon, you do so at your own risk. Personnel may change any part of Marketicon at any time without notice and without any liability. So far as permitted by law, Personnel will not be liable for any direct, indirect, incidental or consequential loss or damage that may arise out of your access, use or inability to use any part of Marketicon.</p>
        <p><strong>5. Limitation of Liability:</strong>
          Personnel will not be liable to you in contract, tort including negligence or otherwise for any indirect, consequential, special or incidental damage or loss arising from your use of or inability to use Marketicon including without limitation, loss of business or profits, loss or corruption of data, loss caused by a virus or software defect, loss of or damage to property, claims of third parties, fines or penalties levied by any taxing or other authority or any other loss or damage. Any reliance you place on information obtained from Marketicon will be at your own risk. You should consider seeking verification and the advice of independent advisers before using information obtained from Marketicon in employment disciplinary or any other matters. The operators of Marketicon its agents and employees, are not liable for any losses or damages arising from your use of this site, other than in respect of death or personal injury caused by their negligence or in respect of fraud.</p>
        <p><strong>6. Data Retention:</strong>
          Our objective is to handle all data Personnelly, fairly and securely. Any information you give us will be stored on Personnel computer systems and may be disclosed to, processed and used by us, and the other companies that assist us in providing our services. This data will be held for a period of approximately 24 months, unless we or you terminate the service, upon which we then may at our discretion delete all your data immediately or any time up to the 24 month period.</p>
        <p><strong>7. User Names and Passwords:</strong>
          It is your responsibility to always keep all User ID and Passwords safe and secret. You must never disclose them or write them down in a way that could be understood by anyone else. You must change any compromised password immediately. The User warrants that any person to whom its user name or password is disclosed is legally authorised to act as the User's agent for the purposes of transacting via the Service.</p>
        <p><strong>8. Payment:</strong>
            The charges will be calculated and invoiced at the end of each calendar month. Payment for all services provided will be collected by Direct Debit or are to be paid within 30 days of invoicing.</p>
        <p><strong>9. Transfer:</strong>
            We reserve the right to transfer this Agreement to any third party at any time. You may not transfer this Agreement to anyone else unless we have agreed in writing beforehand and we shall not unreasonably withhold such agreement.</p>
        <p><strong>10. External Links:</strong>
          Links contained in Marketicon may lead to other websites. Although checked regularly, they are not under our control. We are not responsible for the content of any linked website and cannot take responsibility for the consequences of your using the information or services on linked websites. We cannot guarantee that these links will work all the time.</p>
        <p><strong>11. Security:</strong>
          Communication across the Marketicon system is via a encrypted tunnel or https over the internet. Use of the internet and email is not a 100% secure communications medium. In the interests of preserving confidentiality in your personal details, we strongly advise that you take this into consideration before you send us any information electronically. You agree that any information you send us will be at your own risk.</p>
        <p><strong>12. Provision of Service:</strong>
          Marketicon may require maintenance or fail without notice. We reserve the right to modify or withdraw, temporarily or permanently any part of Marketicon without notice to You and You confirm that we shall not be liable to You or any third party for any modification to or withdrawal. You will not be eligible for any compensation because you cannot use any part of Marketicon or because of a failure, suspension or withdrawal of all or part of Marketicon.</p>
        <p><strong>13. Termination:</strong>
          We will be entitled to suspend access to the Marketicon Service (i.e. restrict your access to the Service for a designated period of time) and/or terminate the Agreement and Service with immediate effect by notice to you, if we are aware of or have reason to believe that any Services or User Id /password used in relation with Marketicon is/are being used in an unauthorised, unlawful, improper or fraudulent way whether this is with your consent or not. You must cancel your agreement with us in writing by a method of sending that produces a proof of receipt by Personnel.</p>
        <p><strong>14. Indemnities:</strong>
          The User agrees to indemnify us against any claims brought by a third party resulting from the User's use of Marketicon and in respect of any losses or liabilities incurred directly by Personnel Service as a result of the User's breach or non-observance of any of these terms and conditions. The User shall pay all costs, damages, awards, fees (including any reasonable legal fees) and judgements awarded against Personnel arising from any such claims and shall provide Personnel with notice of such claims, full authority to defend, compromise or settle such claims and reasonable assistance necessary to defend such claims, at the Users sole expense. The provisions of this paragraph 7 shall survive any termination of this agreement.</p>
        <p><strong>15. Orders:</strong>
          You warrant that the User Information which you are required to provided to Marketicon is true, current and complete in all respects. You also agree to update the User Information of any changes as soon as they occur.</p>
          <p><strong></strong>
            The onus is entirely on you to have a verified, recoverable back up and Anti-virus procedure in place. We make every effort to check and test Marketicon. We cannot accept any responsibility for any damage to your computer system or loss of data caused by using Marketicon. You must not knowingly or recklessly transmit via Marketicon any material which contains viruses or other code or defects which are likely to cause damage to any system or data or engage in any other activity which is in breach of the Computer Misuse Act 1990.</p>
          <p><strong>17. Data Protection:</strong>
            The User agrees that the User will, when using the Service, observe the provisions of the Data Protection Act 1984 and subsequent amending legislation.</p>
          <p><strong>18. Illegal Activity:</strong>
            The User must not use Marketicon to engage in activity which is in breach of any applicable national or international laws.</p>
          <p><strong>19. Disclaimer of Agency:</strong>
            Nothing in this Agreement shall be construed to create a joint venture, partnership or agency relationship between the User and Personnel. The parties to this Agreement are independent contractors and neither party shall have the right or authority to incur any liability debt or cost or enter into any contracts or other arrangements in the name of or on behalf of the other.</p>
          <p><strong>20.Entire Agreement:</strong>
            The terms and conditions contained in this Agreement constitute the entire agreement between Personnel and you, the User and supersedes all other communications.</p>
          <p><strong>21. Acceptance:</strong>
            The User acknowledges that the User has read and accepted the terms of this Agreement. Use of the Service shall be deemed as acceptance of the terms of this Agreement. If you do not agree to be bound by these terms and conditions, you may not use or access Marketicon. If at any time you do not agree to these terms please leave the Marketicon website immediately.</p>
          <p><strong>22. Applicable Law:</strong>
            This Agreement shall be governed by and construed in accordance with the laws of England and Wales and both parties hereby submit to the exclusive jurisdiction of the courts of England and Wales. Revised 01 September 2013.</p>
    </div>
  </div>


</div>


