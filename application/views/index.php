<!DOCTYPE html>
<html lang="en">
<body>

<?php include_once('includes/header.php') ?>

<div class="banner">
  <div class="col-12 text-center main-heading">
    <h1 style="color: white;"><span style="color: #00bb7e;">Free DBS Check:</span> Disclosure and Barring Service</h1>
  </div>

  <div class="container">
    <div class="row py-5">

      <div class="col-8 fg-heading text-center">
        <p>We provide Disclosure and Barring Service (DBS) checks, to help organisations undertake security checks of those working for them, ensuring that their customers are protected from harm.</p>

        <div class="row py-5">

          <div class="col-6 offset-md-3">
            <a href="form.php"><button class="btn btn-danger btn-block p-3">Getting started</button></a>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="row">
          <div class="col-12">
            <div class="side-div">
              <h6>1 Day, 0 Hours, 30 Minutes, 45 Seconds</h6>
              <p>Our fastest DBS result returned so far in January Last updated on 09/01/2021</p>
            </div>
          </div>
          <div class="col-12">
            <div class="side-div">
              <h6>1 Day, 0 Hours, 29 Minutes, 7 Seconds</h6>
              <p>Our fastest DBS result returned so far in December Last updated on 09/01/2021</p>
            </div>
          </div>
          <div class="col-12">
            <div class="side-div">
              <h6>1 Day, 0 Hours, 30 Minutes, 45 Seconds</h6>
              <p>Our fastest DBS result returned so far in 2021 Last updated on 09/01/2021</p>
            </div>
          </div>
        </div>

      </div>


    </div>

  </div>


</div>

<section class="about-section py-5">
  <div class="container text-center">
    <p>We provide Disclosure and Barring Service (DBS) checks, to help organisations undertake security checks of those working for them, ensuring that their customers are protected from harm.
    We can also provide individual support on a face to face basis, and develop specific solutions to individual challenges faced by you (including training courses).
    </p>
    <p>There are three types of DBS checks, which are only available for applicants who are 16 or over:</p>

    <h2>Not Looking for a DBS Check? You Can View Our Other Services Here:</h2>
    <ul style="list-style-type: circle; text-align: left; padding-right: 30px;">
      <li>Standard - This will check for spent and unspent convictions, cautions, reprimands and final warnings.</li>
      <li>Enhanced - This includes the same as the standard check plus any additional information held by local police that is reasonably considered relevant to the workforce being applied for (adult, child or ‘other’ workforce). ‘Other’ workforce means those who don’t work with children or adults specifically, but potentially both, eg taxi drivers. In this case, the police will only release information that is relevant to the post being applied for. </li>
      <li>Enhanced with list checks - This is like the enhanced check, but includes a check of the DBS barred lists. An employer can only ask for a barred list check for specific roles. It’s a criminal offence to ask for a check for any other roles</li>
    </ul>

    <div class="row">
      <div class="col-6 text-center">
        <button class="btn btn-success">Online Trainiing Courses</button>
      </div>
      <div class="col-6 text-center">
        <button class="btn btn-success ">Our CX Products</button>
      </div>
    </div>
  </div>
</section>

<section class="DBS-check text-center">
  <div class="container">
    <h2>What are DBS Checks?</h2>
    <p>DBS (Disclosure and Barring Service) checks are requests to the UK police service to disclose any information they hold on an applicant. This allows you to know an individual's criminal record if they have one. <br>
      They are often carried out by people who are employing staff, and if this is the case for you, more information can be viewed on our <span style="color: #00bb7e;">DBS Checks for Employers</span> page. <br>
      There are three levels of disclosure: <span style="color: #00bb7e;">Basic DBS check</span>,
      <span style="color: #00bb7e;">Standard DBS check</span>, and <span style="color: #00bb7e;">Enhanced DBS check</span>.
    </p>
    <div class="col-12 mb-3">
      <img src="assets/img/asset1.jpg" alt="" style="width: 100%;">
    </div>

    <button class="btn btn-danger">Regiser for free today!</button>

  </div>
</section>
  <!-- footer section  -->
  <?php include_once('includes/footer.php') ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>