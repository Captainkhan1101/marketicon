<?php $this->load->view('include/header'); ?>
<?php $this->load->view('include/aside'); ?>
<?php $this->load->view('include/top_nav'); ?>

<!-- page content -->
<div class="right_col" role="main">
   <div class="">
      <div class="page-title">
      <div class="title_left">
            <h3>Pending Application List</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="success_message"> </div>
            <div id="error_message"> </div>
            <div class="x_panel">
                 <!-- <form class="form-inline">
                  <label>Search</label>
                  <input class="form-control form-control-sm ml-3 w-75" type="text" id="submitted_filter" name="categories_filter" placeholder="Search" aria-label="Search">
                  <input type="hidden" id="submitted_data" value="1" >
               </form> -->
               <div class="x_content">
                    <div id="filter_category_div"> 
                  <table id="datatable-fixed-header" class="table table-striped table-bordered">
                     <thead>
                        <tr class="headings">
                           <th class="column-title">Id </th>
                           <th class="column-title">Name </th>
                           <th style="width:200px !important;">Email</th>
                           <th class="column-title">Contact Number</th>
                           <th class="column-title">Category</th>
                           <th class="column-title">Form status</th>
                           <th class="colum-title">CV</th>
                           <th class="column-title">Form Details</th>
                           <th class="column-title">Change By</th>

                           <th style="width:200px !important;"> Status</th>

                           <th class="column-title no-link last"><span class="nobr">Change Status</span></th>

                           <?php if ($this->session->userdata('user_role') === 'admin') { ?>
                              <th><span>Action</span></th>
                           <?php } ?>

                        </tr>
                     </thead>
                     <div id="main_application_section">
                        <tbody>

                           <?php foreach ($record as $_record) {

                              $categoryName = $this->Base_model->getAll('categories', '', "categoryId   = " . $_record['categoryId']);
                              $status_changed_by = $this->Base_model->getAll('user', '', "user_id   = " . $_record['status_changed_by']);

                           ?>
                              <tr class=" pointer">

                                 <td class=" ">
                                    <?php echo $_record['applicant_id']; ?>
                                 </td>
                                 <td class=" ">
                                    <?php echo $_record['first_name']; ?>
                                 </td>

                                 <td class=" "><?php echo $_record['applicant_email']; ?></td>
                                 <td class=" "><a href=""><?php echo $_record['applicant_contact']; ?></a></td>
                                 <td class=" "><?php echo @$categoryName[0]['categoryName'] ?></td>

                                 <?php if ($_record['form_submited'] == 1) { ?>
                                    <td style="color: green;font-size: 11px;"> submited</td>
                                 <?php } else { ?>
                                    <td style="color: red;font-size: 9px;"> Not Submited</td>
                                 <?php } ?>
                                 <td>
                                    <a target="_blank" href="<?php echo base_url(); ?>assets/job_applications/<?php echo $_record['resume_path']; ?>">View</a>
                                 </td>



                                 <?php if ($_record['form_submited'] == 1) { ?>
                                    <td> <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>HomeController/application_formDetails/<?php echo $_record['applicant_id']; ?>">View</a> </td>
                                 <?php } else { ?>
                                    <td> <a class="btn btn-info btn-xs" disabled href="#">View</a> </td>
                                 <?php } ?>

                                 <td class=" "><?php echo @$status_changed_by[0]['name'] ?></td>
                                 <td><?php if (($_record['application_status']) == 'done') { ?>
                                       <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i> </small>
                                       <small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-check-circle"></i> </small>
                                       <small style="background-color:#398e39; padding:5px; color:white;">Done <i style="color:white" class="fa fa-check-circle"></i></small>

                                    <?php } else if (($_record['application_status']) == 'pending') { ?>
                                       <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i class="fa fa-arrow-right" aria-hidden="true"></i> </small>

                                    <?php } else if (($_record['application_status']) == 'mailsent') { ?>
                                       <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-arrow-right"></i></small>

                                    <?php } else if (($_record['application_status']) == 'called') { ?>
                                       <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#d28f14; padding:5px; color:white;">Called <i class="fa fa-arrow-right" aria-hidden="true"></i></small>

                                    <?php } else if (($_record['application_status']) == 'rejected') { ?>
                                       <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-check-circle"></i></small>
                                       <small style="background-color:red; padding:5px; color:white;">Rejected</small>
                                    <?php } ?>
                                 </td>

                                 <td>
                                    <!-- <a href="<?php //echo base_url();
                                                   ?>HomeController/status_pending/<?php echo  $_record['applicant_id'] ?> " data-toggle="tooltip" title="Change to pending!"><i style="color:#2675b9" class="fa fa-clock-o" aria-hidden="true"></i></a>&nbsp;&nbsp; |&nbsp; -->

                                    <?php if ($_record['application_status'] === 'pending') { ?>

                                       <a href="<?php echo base_url(); ?>HomeController/status_called/<?php echo  $_record['applicant_id'] ?> " data-toggle="tooltip" title="Change to called!"><i style="color:orange" class="fa fa-phone "></i></a>

                                       &nbsp;&nbsp; &nbsp; <a href="#" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" title="Change to Email Link sent!"><i style="color:#2196f3" class="fa fa-envelope user_data" id="<?php echo  $_record['applicant_id'] ?>"></i></a>

                                       <!-- &nbsp;&nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_done/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="change to Application Completed"><i style="color:#4caf50" class="fa fa-check-circle"></i></a>
                                 &nbsp;&nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_rejected/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="Application status Rejected!"><i style="color:red" class="fa fa-ban"> </i></a> -->

                                    <?php } else if ($_record['application_status'] === 'called') { ?>

                                       &nbsp;&nbsp;&nbsp;
                                       <a href="#" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" title="Change to Email Link sent!"><i style="color:#2196f3" class="fa fa-envelope user_data" id="<?php echo  $_record['applicant_id'] ?>"></i></a>
                                       <!-- &nbsp;&nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_done/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="change to Application Completed"><i style="color:#4caf50" class="fa fa-check-circle"></i></a>
                                 &nbsp;&nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_rejected/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="Application status Rejected!"><i style="color:red" class="fa fa-ban"> </i></a> -->

                                    <?php } else if ($_record['application_status'] === 'mailsent') { ?>

                                       <!-- <a href="<?php //echo base_url();
                                                      ?>HomeController/status_pending/<?php//echo  $_record['applicant_id']?> " data-toggle="tooltip" title="Change to called!"><i style="color:blue" class="fa fa-phone"></i></a>  -->
                                       &nbsp;&nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_done/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="change to Application Completed"><i style="color:#4caf50" class="fa fa-check-circle"></i></a>
                                       &nbsp; &nbsp;<a href="<?php echo base_url(); ?>HomeController/status_rejected/<?php echo  $_record['applicant_id'] ?>" data-toggle="tooltip" title="Application status Rejected!"><i style="color:red" class="fa fa-ban"> </i></a>
                                       &nbsp;
                                       <a href="#" data-toggle="modal" data-target="#exampleModal" data-toggle="tooltip" title="Change to Email Link Resent!"><i style="color:#2196f3" class="fa fa-envelope user_data" id="<?php echo  $_record['applicant_id'] ?>"></i></a>

                                    <?php } else if ($_record['application_status'] === 'done') { ?>

                                    <?php } else if ($_record['application_status'] === 'rejected') { ?>

                                    <?php  } ?>
                                 </td>

                                 <?php if ($this->session->userdata('user_role') === 'admin') { ?>
                                    <td>
                                       <a href="<?php echo base_url(); ?>HomeController/delete/<?php echo $_record['applicant_id'] ?>" data-toggle="tooltip" title="Delete Application"><i class="fa fa-trash-o"></i></i></a> &nbsp;
                                    </td>
                                 <?php } ?>

                                 </td>
                              </tr>
                           <?php } ?>

                        </tbody>
                     </div>
                  </table>
                  </div>
                    <div>
                  
                    <div class="row">
                        <div class="col-md-6"><?php echo "Showing ". $per_page. " record of ". $rows_count ?></div>
                        <div class="col-md-6"><span class="pull-right"><?php echo $links ?></span></div>
                      </div>
                    </div>
                 
                  </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>




<?php $this->load->view('include/footer'); ?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Choose Form</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form method="POST">
            <div class="modal-body">
               <div id="applicantDiv"></div>
               <span style="display:none; color:red" id="form_validate">Please choose form</span>
               <select name="form_type" id="form_type" class="form-control" required="required">
                  <option value="">Choose Form</option>
                  <option value="standard_form">Standard Form</option>
                  <option value="enhanced_form">Enhanced Form</option>
               </select>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               <a id="sendEmail" style="display:none" class="btn btn-primary" onclick="status_linksent();">Send Email</a>
            </div>
         </form>
      </div>
   </div>
</div>

<script type="text/javascript">
   $(".user_data").click(function() {
      var applicant_id = $(this).attr('id');
      // alert(applicant_id) ;
      if (applicant_id) {
         $('#sendEmail').show();
         $('#applicantDiv').html('<input type="hidden" name="applicant_id" id="applicant_id_val" value="' + applicant_id + '">');
      } else {
         $('#sendEmail').hide();
      }
   });

   function status_linksent() {

      var applicantID = $('#applicant_id_val').val();
      var form_type = $('#form_type').val();
      if ((form_type) === '') {
         $('#form_validate').show();
         return false;
      }

      $.ajax({
         url: '<?php echo base_url(); ?>HomeController/status_linksent',
         type: 'POST',
         data: {
            applicant_id: applicantID,
            form_type: form_type
         },
         success: function(data) {
            var html = ' <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>' +
               '<small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i></small>' +
               '<small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-arrow-right"></i></small>';

            $('#applicantStatus_' + applicantID).html(html);

            var alertMessage = '<div  class="alert alert-success alert-dismissible fade in" role="alert">' +
               '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
               '<strong>Congratulation ! </strong> Application ID =' + applicantID + ' Form email sent successfully.</div>';
            $('#success_message').html(alertMessage);
            $('#form_validate').hide();
            $('.linkSent_' + applicantID).hide();
            $('#exampleModal').modal('hide');
            $('#call_' + applicantID).hide();

            $('#done_' + applicantID).show();
            $('#rejected_' + applicantID).show();

            setTimeout(function() {
               $(".alert").alert('close');
            }, 8000);

         }
      })


   }

   function status_called(applicantID) {
      // alert(applicantID);
      if (applicantID) {
         $.ajax({
            url: '<?php echo base_url(); ?>HomeController/status_called',
            type: 'POST',
            data: {
               applicantID: applicantID
            },
            success: function(data) {
               var html = '<small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small><small style="background-color:#d28f14; padding:5px; color:white;">Called <i class="fa fa-arrow-right" aria-hidden="true"></i></small>';
               $('#applicantStatus_' + applicantID).html(html);
               $('#call_' + applicantID).hide();

               var alertMessage = '<div  class="alert alert-success alert-dismissible fade in" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                  '<strong>Congratulation ! </strong> Application ID =' + applicantID + ' Status has been called successfully.</div>';
               $('#success_message').html(alertMessage);

               setTimeout(function() {
                  $(".alert").alert('close');
               }, 8000);

            }
         });
      }
   }

   function status_done(applicantID) {
      // alert(applicantID);
      if (applicantID) {
         $.ajax({
            url: '<?php echo base_url(); ?>HomeController/status_done',
            type: 'POST',
            data: {
               applicantID: applicantID
            },
            success: function(data) {
               var html = ' <small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>' +
                  '<small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i> </small>' +
                  '<small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-check-circle"></i> </small>' +
                  '<small style="background-color:#398e39; padding:5px; color:white;">Done <i style="color:white" class="fa fa-check-circle"></i></small>';
               $('#applicantStatus_' + applicantID).html(html);
               $('#done_' + applicantID).hide();
               $('#rejected_' + applicantID).hide();
               $('.linkSent_' + applicantID).hide();
               $('#call_' + applicantID).hide();
               var alertMessage = '<div  class="alert alert-success alert-dismissible fade in" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                  '<strong>Congratulation ! </strong> Application ID =' + applicantID + ' Status has been Completed successfully.</div>';
               $('#success_message').html(alertMessage);

               setTimeout(function() {
                  $(".alert").alert('close');
               }, 8000);

            }
         });
      }
   }

   function status_rejected(applicantID) {
      // alert(applicantID);
      if (applicantID) {
         $.ajax({
            url: '<?php echo base_url(); ?>HomeController/status_rejected',
            type: 'POST',
            data: {
               applicantID: applicantID
            },
            success: function(data) {
               var html = '<small style="background-color:#2675b9; padding:5px; color:white;">Pending <i style="color:white" class="fa fa-check-circle"></i></small>' +
                  '<small style="background-color:#d28f14; padding:5px; color:white;">Called <i style="color:white" class="fa fa-check-circle"></i></small>' +
                  '<small style="background-color:#2196f3; padding:5px; color:white;">Sent <i style="color:white" class="fa fa-check-circle"></i></small>' +
                  '<small style="background-color:red; padding:5px; color:white;">Rejected</small>';
               $('#applicantStatus_' + applicantID).html(html);
               $('#done_' + applicantID).hide();
               $('#rejected_' + applicantID).hide();
               $('.linkSent_' + applicantID).hide();
               $('#call_' + applicantID).hide();

               var alertMessage = '<div  class="alert alert-success alert-dismissible fade in" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                  '<strong>Congratulation ! </strong> Application ID =' + applicantID + ' Status has Rejected</div>';
               $('#success_message').html(alertMessage);

               setTimeout(function() {
                  $(".alert").alert('close');
               }, 8000);
            }
         });
      }
   }

   function delete_application(applicantID) {
      var result = window.confirm('Are you sure you want to delete the application?');
      if (result === true) {
         $.ajax({
            url: '<?php echo base_url(); ?>HomeController/delete',
            type: 'POST',
            data: {
               applicantID: applicantID
            },
            success: function(data) {

               var alertMessage = '<div  class="alert alert-success alert-dismissible fade in" role="alert">' +
                  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>' +
                  ' Application ID =' + applicantID + ' has deleted successfully.</div>';
               $('#success_message').html(alertMessage);

               // remove row from datatable as well
               $('#row_' + applicantID).remove();

               setTimeout(function() {
                  $(".alert").alert('close');
               }, 8000);
            }
         })
      }

   }

   $(window).load(function() {
      $('table').removeClass('loading');
   });
</script>

<script type="text/javascript">
   $('.table').dataTable({
      "searching": true,
      "bInfo": false,
      "paging": true,
      "bPaginate": false,
      "order": [
         [4, "desc"]
      ],
   })
   
</script>