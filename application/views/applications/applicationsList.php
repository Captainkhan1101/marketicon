<?php  $this->load->view('include/header'); ?>
<?php  $this->load->view('include/aside'); ?>
<?php  $this->load->view('include/top_nav'); ?>
<!-- page content -->
<!-- page content -->
<div class="right_col" role="main">
   <div class="">
      <div class="page-title">
         <div class="title_left">
            <h3>Application List</h3>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
         <?php if($this->session->flashdata('flash_msg_yes')){ ?>
<div class="alert alert-success alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Congratulation ! </strong> <?php echo $this->session->flashdata('flash_msg_yes'); ?>
</div>
<?php }?>
<?php if($this->session->flashdata('flash_msg_no')){ ?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-exclamation-triangle"></i> <?php echo $this->session->flashdata('flash_msg_no'); ?>
</div>
<?php }?>
            <div class="x_panel">
               <div class="x_content bs-example-popovers">
               </div>
               <div class="x_content">
               <div id="filter_category_div"> 
                  <form name="del-title" id="del-title" action="delete_bulk/" method="post">
                     <div class="table-responsive">
                        <table id="datatable-checkbox " class="table jambo_table table-striped bulk_action example">
                           <thead>
                              <tr class="headings">
                                 <th class="column-title">Id </th>
                                 <th class="column-title">Full Name </th>
                                 <!--<th class="column-title">Email </th>  -->
                                 <!-- <th class="column-title">Contact no </th>   -->
                                 <th class="column-title">View Form Details </th>  
                                 <?php if($this->session->userdata('user_role') ==='admin'){?>   
                                    <th><span >Action</span></th>
                                 <?php }?>
                              </tr>
                           </thead>
                           <div id="main_application_section">
                           <tbody>
                           <?php foreach($record as $_record){
                               $categoryName=$this->Base_model->getAll('categories','',"categoryId   = ".$_record['categoryId']);
                               $status_changed_by=$this->Base_model->getAll('user','',"user_id   = ".$_record['status_changed_by']);
                              ?>                        
                              <tr class=" pointer">
                                 <td class=" "><a href=""><?php echo $_record['applicant_id'];?></a></td>
                                 <td class=" "><a href=""><?php echo $_record['first_name'];?></a></td>
                                 <!--<td class=" "><a href=""><?php echo $_record['applicant_email'];?></a></td>-->
                                 <!-- <td class=" "><a href=""><?php echo $_record[''];?></a></td> -->
                           <td>
                           <a  class="btn btn-info btn-xs" href="<?php echo base_url();?>HomeController/application_formDetails/<?php echo $_record['applicant_id'];?>">View</a>
                           </td>
                          <?php if($this->session->userdata('user_role') ==='admin'){?>   
                              <td >
                                 <a href="<?php echo base_url();?>HomeController/delete/<?php echo $_record['applicant_id']?>" data-toggle="tooltip" title="Delete Application"><i class="fa fa-trash-o"></i></i></a>  &nbsp;
                              </td>
                           <?php }?>
                            </td>                               
                              </tr>
                           <?php } ?>
                           </tbody>
                           </div>
                        </table>
                     </div>
                  </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   <?php $this->load->view('include/footer'); ?>
   <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choose Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
         <form action="<?php echo base_url();?>HomeController/status_linksent" method="POST">
            <div class="modal-body">
            <div id="applicantDiv"></div>
            <select name="form_type" id="form_type" class="form-control" required="required">
            <option value="">Choose Form</option>
            <option value="standard_form">Standard Form</option>
            <option value="enhanced_form">Enhanced Form</option>
            </select>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send Email</button>
            </div>
         </form>
    </div>
  </div>
</div>
<script type="text/javascript">
      $(".user_data").click(function(){
         var applicant_id = $(this).attr('id');        
         $('#applicantDiv').html('<input type="hidden" name="applicant_id" value="'+applicant_id+'">');
      });
      $('.example').dataTable( {
        "order": [[ 'Id', "desc" ]],
    } );
</script>