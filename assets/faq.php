<?php include_once('includes/header.php') ?>

<body>
  <div class="container">
    <div class="row">
      <div class="col-12 text-center py-5">
        <h1>FAQ</h1>
        <p class="para text-center">Here are the most frequently asked questions we are asked. Can’t find the answer to your question? Feel free to give us a call, or send us an email - We’ll be more than happy to help!</p>
      </div>
    </div>

    <div class="row">
      <div class="col-6">
        <ul class="faq-links pl-5">
          <li><a href="#one">Who has access to my personal information and DBS disclosure?</a></li>
          <li><a href="#two">Can my application be tracked?</a></li>
          <li><a href="#three">If I have a criminal conviction, can I still be employed?</a></li>
        </ul>
      </div>
      <div class="col-6">
      <ul class="faq-links pl-5">
          <li value="4"><a href="#four">How do I know if the job I am applying for is required to provide details of all convictions, spent or unspent?</a></li>
          <li><a href="#five">Can I refuse to apply for a DBS check?</a></li>
          <li><a href="#six">If I am recruited from abroad, will I still be checked for criminal records?</a></li>
        </ul>
      </div>
    </div>

    <div class="row py-5 faq-details">
      <div class="col-12" id="one">
        <h3>1. Who has access to my personal information and DBS disclosure?</h3>
        <p>Your personal information will only be seen by those whose jobs require them to do so in the course of their duties.</p>
      </div>

      <div class="col-12" id="two">
        <h3>2. Can my application be tracked?</h3>
        <p>Your application can be tracked via the tracking system on the <a href="https://www.gov.uk/disclosure-barring-service-check/overview">DBS website</a>. Applicants will need to provide their disclosure application form reference number and their date of birth.</p>
      </div>

      <div class="col-12" id="three">
        <h3>3. If I have a criminal conviction, can I still be employed?</h3>
        <p>This will depend on whether your offence is considered to make you unsuitable to have access to patients. We conduct a greater level of checks on staff who work with certain patient groups, such as children and vulnerable adults. We will however consider a range of factors before making our decision to appoint -the nature of the offence -the age at which it was committed - its relevance to the post in question - whether the applicant has a pattern of offending behaviour - whether the applicants circumstances have changed since the offending behaviour - the circumstances surrounding the offence and the explanation(s) offered by the convicted individual.</p>
      </div>

      <div class="col-12" id="four">
        <h3>4. How do I know if the job I am applying for is required to provide details of all convictions, spent or unspent?</h3>
        <p>There are some jobs which are not covered by the Rehabilitation of Offenders Act 1974. These are jobs which are positions of trust and are ones which involve a far greater degree of contact with children or vulnerable adults, e.g. a nurse working on a children's' ward or a cleaner on a children's' ward. For these types of jobs, the employer is entitled to see a person's full criminal history in order to assess their suitability for a position.</p>
      </div>

      <div class="col-12" id="five">
        <h3>5. Can I refuse to apply for a DBS check?</h3>
        <p>Yes. However, there are some posts for which a DBS check is required by law. If you refuse to apply for a DBS check in this instance, the organisation may not be able to take your job or licence application any further.</p>
      </div>

      <div class="col-12" id="six">
        <h3>6. If I am recruited from abroad, will I still be checked for criminal records?</h3>
        <p>Yes - we will carry out necessary police checks in line with that country's justice system and UK requirements</p>
      </div>

    </div>
  </div>
</body>
<?php include_once('includes/footer.php')?>