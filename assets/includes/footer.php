
<section class="contact-us py-3">
  <div class="container">
    <div class="row">
      <div class="col-8 p-5 form-deign col-sm-7">
        <form action="mail.php" method="post">
          <div class="row">
            <div class="col-12">
              <h4>Send Us A Message</h4>
            </div>
            <div class="col-6 pb-2">
              <input name="first_name" type="text" class="btn-block" placeholder="First Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="last_name" type="text" class="btn-block" placeholder="Last Name *">
            </div>
            <div class="col-6 pb-2">
              <input name="email" type="text" class="btn-block" placeholder="Email *">
            </div>
            <div class="col-6 pb-2">
              <input name="phone_number" type="text" class="btn-block" placeholder="Your Phone (optional)">
            </div>
            <div class="col-12 pb-2">
              <textarea name="message" id="" cols="10" rows="5" class="btn-block" placeholder="Your Message *"></textarea>
            </div>
            <div class="col-3 pb-2 col-sm-6">
              <button class="btn btn-dark" type="submit" name="submit">SEND MESSAGE</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-4 contact-info col-sm-5">
        <div class="row">
          <div class="col-2 pt-5">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-headphones fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-5">
              <p>CUSTOMMER SUPPORT</p>
              <p>02080883560</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-envelope-o fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>EMAIL ADDRESS</p>
              <p>contact@marketicon.co.uk</p>
          </div>

          <div class="col-2 pt-3">
            <span class="fa-stack fa-lg">
              <i class="fa fa-circle fa-stack-2x" style="color: white;"></i>
              <i class="fa fa-map-marker  fa-stack-1x fa-inverse" style="color: black;"></i>
            </span>
          </div>
          <div class="col-9 text-white pt-3">
              <p>OFFICE ADDRESS</p>
              <p>Spaces Heathrow Airport
                4 Roundwood Avenue
                Stockley Park
                UXBRIDGE
                UB11 1AF
                </p>
          </div>

        </div>
      </div>
    </div>

  </div>
</section>

<footer class="footer-section py-5">
  <div class="container">
  <div class="row">
    <div class="col-3">
     <a href="index.php"> <img src="assets/img/marketicon.png" alt="" style="width: 80%;"></a>
    </div>
    <div class="col-3 text-white">
      <h5>SUPPORT</h5>
      <ul class="footer-links">
        <li><a href="">FAQs</a></li>
        <li><a href="form.">Apply Now</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
    <h5>INFORMATION</h5>
    <ul class="footer-links">
        <li><a href="">Data Protection Policy</a></li>
        <li><a href="">Privacy Policy</a></li>
        <li><a href="terms-condition.php">Terms and Conditions</a></li>
      </ul>
    </div>
    <div class="col-3 text-white">
      <img src="assets/img/Cyber-Essentials-Badge-Large-72dpi.png" alt="" style="width: 50%;">
    </div>
  </div>

  <div class="row">
    <div class="col-6 text-white">
      <p>© Copyright. All rights reserved. Marketicon.</p> <span></span>
    </div>
    <div class="col-6 text-center text-white">
      <p>Company number: 12691478</p> <span></span>
    </div>
  </div>
  </div>
</footer>

