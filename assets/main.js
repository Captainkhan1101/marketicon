    $('#date').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date1').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date2').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date3').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date4').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date5').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date6').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date7').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date8').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date9').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date10').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date11').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date12').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date13').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date14').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date15').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date16').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd/mm/yy',
      yearRange: '1950:2050'
   }).val();
   $('#date17').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'mm/yy',
      yearRange: '1950:2050'
   }).val();

   $('#date18').datepicker({
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      dateFormat: 'mm/yy',
      yearRange: '1950:2050'
   });

// Hide sections on load
   $("#license_section").hide();
   $("#passport_section").hide();

      $('input[type=radio][name=hasValidLicence]').on('change', function() {
  switch ($(this).val()) {
    case 'yes':
      //alert("has license");
      $("#license_section").show();
      break;
    case 'no':
      //alert("No license");
      $("#license_section").hide();
      break;
  }
});

$('input[type=radio][name=hasValidPassport]').on('change', function() {
  switch ($(this).val()) {
    case 'yes':
      //alert("has passport");
      $("#passport_section").show();
      break;
    case 'no':
      //alert("No passport");
      $("#passport_section").hide();
      break;
  }
});

$("#address1").hide();
$("#address2").hide();
$("#address3").hide();
$("#address4").hide();
$("#address5").hide();

$("#btn_add_address1").on('click', function(){
$("#address2").slideDown();
});

$("#btn_add_address2").on('click', function(){
$("#address3").slideDown();
$("#btn_add_address2").fadeOut();
$("#btn_remove_address2").fadeOut();
});

$("#btn_remove_address2").on('click', function(){
$("#address2").slideUp();
$("#btn_add_address2").show();
$("#btn_remove_address2").show();
});

$("#btn_add_address3").on('click', function(){
$("#address4").slideDown();
$("#btn_add_address3").fadeOut();
$("#btn_remove_address3").fadeOut();
});

$("#btn_remove_address3").on('click', function(){
$("#address3").slideUp();
$("#btn_add_address3").show();
$("#btn_remove_address3").show();
$("#btn_add_address2").show();
$("#btn_remove_address2").show();
});

$("#btn_add_address4").on('click', function(){
$("#address5").slideDown();
$("#btn_add_address4").fadeOut();
$("#btn_remove_address4").fadeOut();
});

$("#btn_remove_address4").on('click', function(){
$("#address4").slideUp();
$("#btn_add_address4").show();
$("#btn_remove_address4").show();
$("#btn_add_address3").show();
$("#btn_remove_address3").show();
});


$("#btn_remove_address5").on('click', function(){
$("#address5").slideUp();
$("#btn_add_address4").show();
$("#btn_remove_address4").show();
$("#btn_remove_address4").show();
});

function diff_years(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
   diff /= (60 * 60 * 24);
  return Math.abs(diff/365.25);
   
 }

 $("#date17").on('change', function(){
 //var enteredDate = new Date("June 13, 2014 08:11:00");

 var enteredDate = $(this).val();

 //alert(enteredDate);

 var arr = enteredDate.split("/");

 var formattedDate = arr[0] +"/" +"01" +"/" + arr[1];

 //alert(formattedDate);

 var diffyears = diff_years(new Date(), new Date(formattedDate))

 //alert(diffyears);

 if(parseFloat(diffyears) < 5)
 {
 //alert("yes");
 $("#address1").slideDown();
 }
 else {
	//alert("no");
    $("#address1").hide();
$("#address2").hide();
$("#address3").hide();
$("#address4").hide();
$("#address5").hide();
}

 });

</script>